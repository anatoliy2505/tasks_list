import { combineReducers } from 'redux';

import {loginServiceReducer} from './loginService/reducer'

export const LOGIN_MANAGMENT_REDUCER_NAMESPACES = {
    loginManagmentRoot: "loginManagment",
    login: "login"
}

export const loginManagmentRootReducer = combineReducers({
    [LOGIN_MANAGMENT_REDUCER_NAMESPACES.login]: loginServiceReducer,
});


