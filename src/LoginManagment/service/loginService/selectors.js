import {createSelector} from 'reselect';

export const loginManagmentRootSelector = state => state.loginManagment.login;

export const loginManagmentSignInStartedSelector = createSelector(
  loginManagmentRootSelector,
  state => Boolean(state && state.singInIsSrarted),
);

export const loginManagmentSignInFinishedSelector = createSelector(
  loginManagmentRootSelector,
  state => Boolean(state && state.singInIsFinished),
);
  
export const loginManagmentTokenSelector = createSelector(
  loginManagmentRootSelector,
  state => state && state.token
);

export const loginManagmentIsAuthSelector = createSelector(
  loginManagmentRootSelector,
  state => Boolean(state && state.isAuth),
); 
