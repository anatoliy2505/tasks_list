import {createSelector} from 'reselect';

export const tasksRootManagmentRootSelector = state => state.rootTasksManagnet;

export const tasksRootManagmentTasksListIsLoadingSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => Boolean(state && state.tasksListIsLoading),
);

export const tasksRootManagmentTasksListSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => (state && state.tasksList) || [],
);

export const tasksRootManagmentTasksTotalPageSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => state && state.tasksTotalPage,
);

export const tasksRootManagmentTasksSortOfFieldSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => state && state.tasksSortOfField,
);

export const tasksRootManagmentTasksSortOfDirectionSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => state && state.tasksSortOfDirection,
);

export const tasksRootManagmentTasksCurrentPageSelector = createSelector(
  tasksRootManagmentRootSelector,
  state => state && state.tasksCurrentPage,
);