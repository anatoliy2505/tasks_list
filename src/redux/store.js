import { createBrowserHistory } from 'history';
import {applyMiddleware, createStore, compose} from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

import {createRootReducers} from './rootCombineReducer';

export const history = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [thunk];

export const store = createStore(
    createRootReducers(history),
    composeEnhancers(
        applyMiddleware(...middleware, routerMiddleware(history))
        )
);
