import {
    TASKS_MANAGMENT_REQUEST_CREATE_TASK_START, 
    TASKS_MANAGMENT_REQUEST_CREATE_TASK_SUCCESS,
    TASKS_MANAGMENT_REQUEST_CREATE_TASK_FAIL,
    TASKS_MANAGMENT_REQUEST_CREATE_TASK_RESET
    } from './actions';

const initialState={
    taskCreationIsLoading: false,
    taskCreationIsLoaded: false    
}

export function taskInterectionServiceReducer(state=initialState, action) {
    switch (action.type) {
        case TASKS_MANAGMENT_REQUEST_CREATE_TASK_START:
          return {
            ...state,
            taskCreationIsLoading: true,
            taskCreationIsLoaded: false,
          };
        case TASKS_MANAGMENT_REQUEST_CREATE_TASK_SUCCESS:
          return {
            ...state,
            taskCreationIsLoading: false,
            taskCreationIsLoaded: true
          };
        case TASKS_MANAGMENT_REQUEST_CREATE_TASK_FAIL:
          return {
            ...state,
            taskCreationIsLoading: false,
            taskCreationIsLoaded: false,
          };
        case TASKS_MANAGMENT_REQUEST_CREATE_TASK_RESET: 
          return {
            ...state,
            taskCreationIsLoading: false,
            taskCreationIsLoaded: false,
        };
        default: 
            return state;
    }
}
