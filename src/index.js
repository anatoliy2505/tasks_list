import React from 'react';
import ReactDOM from 'react-dom';

import { ConnectedRouter } from 'connected-react-router';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import {store, history} from './redux/store';

import App from './App';

import './index.css';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(    
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Router>
                <App />
            </Router>
        </ConnectedRouter>        
    </Provider>
, document.getElementById('root'));


serviceWorker.unregister();
