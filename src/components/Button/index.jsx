import React from 'react'
import PropTypes from 'prop-types'

import './Button.scss'

const Button = props =>
    (
    <button {...props} className='button'>{props.children}</button>
    )


Button.propTypes = {
    className: PropTypes.string,
}

export default Button