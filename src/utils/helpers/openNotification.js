import { notification } from 'antd';

export default ({type = 'info', title, text}) => {
    notification[type]({
        message: title,
        description: text
    })
};