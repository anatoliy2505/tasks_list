import {LOGIN_MANAGMENT} from '../../constantns';
import {START, SUCCESS, FAIL, RESET} from '../../../common/constans';

import openNotification from '../../../utils/helpers/openNotification';

import axios from 'axios';

export const REQUEST_SIGNIN = "REQUEST_SIGNIN";
export const LOGIN_MANAGMENT_REQUEST_SIGNIN_START = LOGIN_MANAGMENT+REQUEST_SIGNIN+START; 
export const LOGIN_MANAGMENT_REQUEST_SIGNIN_SUCCESS = LOGIN_MANAGMENT+REQUEST_SIGNIN+SUCCESS;
export const LOGIN_MANAGMENT_REQUEST_SIGNIN_FAIL = LOGIN_MANAGMENT+REQUEST_SIGNIN+FAIL;
export const LOGIN_MANAGMENT_REQUEST_SIGNIN_RESET = LOGIN_MANAGMENT+REQUEST_SIGNIN+RESET;

export const REQUEST_SIGNOUT = "REQUEST_SIGNOUT";
export const LOGIN_MANAGMENT_REQUEST_SIGNOUT = LOGIN_MANAGMENT+REQUEST_SIGNOUT; 

export const signIn = ({login, password}) => dispatch => {
    dispatch({ type: LOGIN_MANAGMENT_REQUEST_SIGNIN_START });
    let form = new FormData();
        form.append("username", login);
        form.append("password", password);

    return axios({
        method: 'post',
        baseURL: 'https://uxcandy.com/~shapoval/test-task-backend/v2',
        url: '/login?developer=Anatoliy',
        mimeType: "multipart/form-data",
        contentType: false,
        processData: false,
        headers: { 'content-type': false },
        responseType: 'json',
        data: form,
        proxy: {
            host: 'https://uxcandy.com'
          }
      }).then(({data}) => {   
          if(data.status === 'ok')  {           
            localStorage.setItem('token', data.message.token);
            dispatch({ type: LOGIN_MANAGMENT_REQUEST_SIGNIN_SUCCESS, token: data.message.toke });
          } else {
            openNotification({
              title: 'Ошибка!',
              text: 'Неверный логин или пароль.',
              type: 'error'
            });
            dispatch({ type: LOGIN_MANAGMENT_REQUEST_SIGNIN_FAIL });
          }            
    }).catch(() => {
        openNotification({
          title: 'Ошибка!',
          text: 'Неудалось отправить данные.',
          type: 'error'
        })
        dispatch({ type: LOGIN_MANAGMENT_REQUEST_SIGNIN_FAIL });
    })
  };

export const signOut = () => dispatch => {
  localStorage.removeItem('token');
  dispatch({type: LOGIN_MANAGMENT_REQUEST_SIGNOUT});
}

export const resetLoginFinishedFlag = () => dispatch => {
  dispatch({type: LOGIN_MANAGMENT_REQUEST_SIGNIN_RESET});
}



 