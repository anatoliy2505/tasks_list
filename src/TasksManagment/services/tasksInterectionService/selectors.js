import {createSelector} from 'reselect';

export const taskManagmentRootSelector = state => state.taskManagment.tasksInteraction;
  
  
  export const taskManagmentTaskCreationIsLoadingSelector = createSelector(
    taskManagmentRootSelector,
    state => Boolean(state && state.taskCreationIsLoading),
  );
  
  export const taskManagmentTaskCreationIsLoadedSelector = createSelector(
    taskManagmentRootSelector,
    state => Boolean(state && state.taskCreationIsLoaded),
  );
  

  
