import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import {loginManagmentIsAuthSelector} from './service/loginService/selectors';
import {adminManagmentTaskChangeFailSelector} from '../AdminManagment/services/taskChangeService/selectors';
import {signOut} from './service/loginService/actions';
import {resetTaskChangedFlag} from '../AdminManagment/services/taskChangeService/actions';

class LoginPage extends Component {    
    
    componentDidMount() {
       if(this.props.isAdmin) {
        this.props.signOut();
        if(this.props.taskChangeFail) {
            this.props.resetTaskChangedFlag();
        }
       }
    }

    render() {
        return (
            <div>
                {!this.props.isAdmin && <Redirect to='/login' />} 
            </div>   
        )
    }
}

export default connect((state) => {
    return {
      isAdmin: loginManagmentIsAuthSelector(state),
      taskChangeFail: adminManagmentTaskChangeFailSelector(state)  
    }
}, {signOut, resetTaskChangedFlag})(LoginPage);
