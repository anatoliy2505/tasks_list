import {ADMIN_MANAGMENT} from '../../constans';
import {START, SUCCESS, FAIL, RESET} from '../../../common/constans';

import openNotification from '../../../utils/helpers/openNotification';

import axios from 'axios';

export const REQUEST_CHANGE_TASK = "REQUEST_CHANGE_TASK";
export const ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_START = ADMIN_MANAGMENT+REQUEST_CHANGE_TASK+START;
export const ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_SUCCESS = ADMIN_MANAGMENT+REQUEST_CHANGE_TASK+SUCCESS;
export const ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_FAIL = ADMIN_MANAGMENT+REQUEST_CHANGE_TASK+FAIL;
export const ADMIN_MANAGMENT_REQUEST_CHANGE_TASKK_RESET = ADMIN_MANAGMENT+REQUEST_CHANGE_TASK+RESET;

export const taskChange = ({id, status, text}) => dispatch => {
    dispatch({ type: ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_START });
    let form = new FormData();
        form.append("token", window.localStorage['token']);
        form.append("status", status);
        form.append("text", text);

    return axios({
        method: 'post',
        baseURL: 'https://uxcandy.com/~shapoval/test-task-backend/v2',
        url: '/edit/'+id+'/?developer=Anatoliy',
        mimeType: "multipart/form-data",
        contentType: false,
        processData: false,
        headers: { 'content-type': false },
        responseType: 'json',
        data: form,
        proxy: {
            host: 'https://uxcandy.com'
          }
      }).then(({data}) => {   
          if(data.status === 'ok')  {
            dispatch({ type: ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_SUCCESS });
          } else {
            openNotification({
              title: 'Ошибка!',
              text: data.message.token + " Для продолжения работы Авторизуйтесь!",
              type: 'error'
            });
            localStorage.removeItem('token');
            dispatch({ type: ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_FAIL });
          }            
    }).catch(() => {
      openNotification({
        title: 'Ошибка!',
        text: 'У вас нет прав для изменения заданий',
        type: 'error'
      });
      dispatch({ type: ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_FAIL });
    })
  };

 export const resetTaskChangedFlag = () => dispatch=> (dispatch({
    type: ADMIN_MANAGMENT_REQUEST_CHANGE_TASKK_RESET
 } ));

 