import React from 'react';
import {Link as MainLink} from 'react-router-dom';

import Button from '../Button';

import './Link.scss';

const Link = ({link, children}) => {
    return (
        <div className="link-container">
            <MainLink to={link}><Button>{children}</Button></MainLink>
        </div>
    )
}

export default Link;