export default  ({errors, values}) => {

    const rules = {

        username: (value) => {
            if (!value) {
                errors.username = 'Введите имя';
            } else if(value.trim().length < 3) {
                errors.username = 'Имя должно содержать более 3 символов';
            }
        },
        email: (value) => {
            if (!value) {
                errors.email = 'Введите E-Mail';
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
                    value.trim()
                )
            ) {
                errors.email = 'Некоррекный E-Mail';
            }
        },
        text: (value) => {
            if (!value) {
                errors.text = 'Введите текст задания';
            } else if(value.trim().length < 1) {
                errors.text = 'Задание должно содержать хотя бы 1 символ';
            }
        },
        login: (value) => {
            if (!value) {
                errors.login = 'Введите login'
            } else if(value.trim().length < 3) {
                errors.login = 'login должен содержать хотя бы 3 символа';
            }
        },
        password: (value) => {
            if (!value) {
                errors.password = 'Введите пароль';

            } else if ( value.trim().length < 3 ) {
                errors.password = 'Пароль должен состоять хотя бы из 3х символов';
            }
        },
        

    }

    Object.keys(values).forEach(
        key => rules[key] && rules[key](values[key])
    )
}




