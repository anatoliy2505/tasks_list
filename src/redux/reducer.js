import {
  ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST,
  ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_START,
  ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_SUCCESS,
  ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_FAIL
    } from './actions';

const initialState={
    tasksSortOfField: 'id',
    tasksSortOfDirection: 'asc',

    tasksTotalCount: 0,
    tasksCurrentPage: 1,
    tasksTotalPage: 0,
    tasksPerPage: 3,

    tasksListIsLoading: false,
    tasksList: []    
}

export function tasksRootReducer(state=initialState, action) {
    switch (action.type) {
  
        case ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_START: 
          return {
            ...state,
            tasksListIsLoading: true
        };
        case ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST: 
          return {                   
            ...state,     
            tasksList: action.tasks,
            tasksTotalCount: action.totalTask,
            tasksTotalPage: Math.ceil(action.totalTask/state.tasksPerPage),
            tasksCurrentPage: action.currentPage,
            tasksSortOfField: action.sortField,
            tasksSortOfDirection: action.sortDirection         
        };
        case ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_SUCCESS: 
          return {
            ...state,
            tasksListIsLoading: false
        };
        case ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_FAIL: 
          return {
            ...state,
            tasksListIsLoading: false
        };        
        default: 
            return state;
    }
}
