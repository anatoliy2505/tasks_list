import React, {Component} from 'react';
import {connect} from 'react-redux';

import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

import {Button} from '../../components';
import openNotification from '../../utils/helpers/openNotification';

import {
    adminManagmentTaskChangeIsLoadedSelector
} from '../services/taskChangeService/selectors';

import {
    taskChange,
    resetTaskChangedFlag
} from '../services/taskChangeService/actions';

class TaskChangeFields extends Component {

    constructor(props) {
        super(props);

        this.state = {
            taskStatus: this.props.status,
            valTask: this.props.text,
            valInitialTask: this.props.text,
            id: this.props.id,
            changedTextFlag: false,
            chengedStatusFlag: false
        };
        this.onChangeCheck = this.onChangeCheck.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onChangeItem = this.onChangeItem.bind(this);
        this.openConfirm = this.openConfirm.bind(this);
        this.resetFlag = this.resetFlag.bind(this);
    }

    componentDidUpdate(prevProps, previousState) {
        if((prevProps.taskChangeIsLoaded 
            !== this.props.taskChangeIsLoaded
            && this.props.taskChangeIsLoaded) && 
            (previousState.changedTextFlag ||  
            previousState.chengedStatusFlag)) {                
                this.props.resetTaskChangedFlag();
                this.resetFlag();
                openNotification({
                    title: 'Успех!',
                    text: 'Задача успешно обновлена!',
                    type: 'success'
                })
            } 
    }

    resetFlag(){
        this.setState({
            changedTextFlag: false,
            chengedStatusFlag: false
        }); 
    }

    openConfirm() {
        if(this.state.changedTextFlag || this.state.chengedStatusFlag) {
            if(this.state.valTask) {
                confirmAlert({
                    title: 'Подтвердите действия',
                    message: 'Вы действительно хотите изменить Задание?',
                    buttons: [
                        {
                        label: 'Да',
                        onClick: () => this.onChangeItem()
                        },
                        {
                        label: 'Нет',
                        onClick: () => {}
                        }
                    ]
                })
            } else {
                openNotification({
                    title: 'Ошибка!',
                    text: 'Текст задания не может быть пустым.',
                    type: 'error'
                })
            }            
        } else {
            openNotification({
                title: 'Ошибка!',
                text: 'Вы не производили изменения.',
                type: 'error'
            })
        }        
    }
     
    onChangeItem() {   
        const {taskStatus, id, valTask} = this.state;
        this.props.taskChange({
            id,
            text: valTask,
            status: taskStatus
        });
    }

    onChangeInput(e) {
        const changedTextFlag = (this.state.valInitialTask === e.target.value) ? false : true;
        this.setState({
            valTask: e.target.value,
            changedTextFlag
        })        
    }

    onChangeCheck(e) {
        this.setState({
            taskStatus: e.target.checked ? 10 : 0,
            chengedStatusFlag: !this.state.chengedStatusFlag
        })
    }

    render() {
        const {taskStatus, valTask} = this.state;
        return (
            <div>
                <div className="todo__task">
                    <input onChange={this.onChangeInput} type="text" value={valTask}/>
                </div>
                <label  className="todo__item" htmlFor="checkTask">Задание выполнено</label> 
                <input  className="todo__item" onChange={this.onChangeCheck} type="checkbox" checked={taskStatus ? true : false }/>        
                <div  className="todo__task">
                    <Button onClick={this.openConfirm}>Сохранить изменение</Button>
                </div>
            </div>
        )    
    }
}
 
export default connect((state) => {
    return {
        taskChangeIsLoaded: adminManagmentTaskChangeIsLoadedSelector(state)
    }
},
{
    taskChange,
    resetTaskChangedFlag
})(TaskChangeFields);