import { combineReducers } from 'redux';

import {taskChangeServiceReducer} from './taskChangeService/reducer'

export const ADMIN_MANAGMENT_REDUCER_NAMESPACES = {
    adminManagmentRoot: "adminManagment",
    taskChange: "taskChange"
}


export const adminManagmentRootReducer = combineReducers({
    [ADMIN_MANAGMENT_REDUCER_NAMESPACES.taskChange]: taskChangeServiceReducer
});


