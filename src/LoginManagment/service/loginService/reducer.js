import {
    LOGIN_MANAGMENT_REQUEST_SIGNIN_START, 
    LOGIN_MANAGMENT_REQUEST_SIGNIN_SUCCESS,
    LOGIN_MANAGMENT_REQUEST_SIGNIN_FAIL,
    LOGIN_MANAGMENT_REQUEST_SIGNIN_RESET,
    LOGIN_MANAGMENT_REQUEST_SIGNOUT
    } from './actions';

const initialState={
    singInIsSrarted: false,   
    singInIsFinished: false,
    isAuth: window.localStorage.token ? true : false,
    token: window.localStorage.token   
}

export function loginServiceReducer(state=initialState, action) {
    switch (action.type) {
      case LOGIN_MANAGMENT_REQUEST_SIGNIN_START:
        return {
          ...state,
          singInIsSrarted: true,
          singInIsFinished: false
        };
      case LOGIN_MANAGMENT_REQUEST_SIGNIN_SUCCESS:
        return {
          ...state,
          token: window.localStorage.token,
          isAuth: window.localStorage.token ? true : false,
          singInIsSrarted: false,   
          singInIsFinished: true
        };
      case LOGIN_MANAGMENT_REQUEST_SIGNIN_FAIL:
        return {
          ...state,
          singInIsSrarted: false,
          singInIsFinished: false
        }; 
      case LOGIN_MANAGMENT_REQUEST_SIGNIN_RESET:
        return {
          ...state,
          singInIsSrarted: false,
          singInIsFinished: false
        }; 
      case LOGIN_MANAGMENT_REQUEST_SIGNOUT:
      return {
        ...state,
        token: '',
        isAuth: false
      };       
      default: 
          return state;
    }
}
