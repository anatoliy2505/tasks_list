import React from 'react';

import LoginForm from './modules/LoginForm';
import {Link} from '../components';

const LoginPage = () => {
    
    return (
        <React.Fragment> 
            <Link link="/">Вернуться на страницу с заданиями</Link>
            <LoginForm />
        </React.Fragment>
    )
}

export default LoginPage;
