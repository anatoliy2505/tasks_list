import {TASKS_MANAGMENT} from '../../constants';
import {START, SUCCESS, FAIL, RESET} from '../../../common/constans';
import axios from 'axios';

export const REQUEST_CREATE_TASK = "REQUEST_CREATE_TASK";
export const TASKS_MANAGMENT_REQUEST_CREATE_TASK = TASKS_MANAGMENT+REQUEST_CREATE_TASK;
export const TASKS_MANAGMENT_REQUEST_CREATE_TASK_START = TASKS_MANAGMENT+REQUEST_CREATE_TASK+START;
export const TASKS_MANAGMENT_REQUEST_CREATE_TASK_SUCCESS = TASKS_MANAGMENT+REQUEST_CREATE_TASK+SUCCESS;
export const TASKS_MANAGMENT_REQUEST_CREATE_TASK_FAIL = TASKS_MANAGMENT+REQUEST_CREATE_TASK+FAIL;
export const TASKS_MANAGMENT_REQUEST_CREATE_TASK_RESET = TASKS_MANAGMENT+REQUEST_CREATE_TASK+RESET;


export const createTask = ({username, email, text}) => dispatch => {
    dispatch({ type: TASKS_MANAGMENT_REQUEST_CREATE_TASK_START });
    let form = new FormData();
        form.append("username", username);
        form.append("email", email);
        form.append("text", text);

    return axios({
        method: 'post',
        baseURL: 'https://uxcandy.com/~shapoval/test-task-backend/v2',
        url: '/create?developer=Anatoliy',
        mimeType: "multipart/form-data",
        contentType: false,
        processData: false,
        headers: { 'content-type': false },
        responseType: 'json',
        data: form,
        proxy: {
            host: 'https://uxcandy.com'
          }
      }).then(({data}) => {   
          if(data.status === 'ok')  {
            console.log(data);
            dispatch({ type: TASKS_MANAGMENT_REQUEST_CREATE_TASK_SUCCESS });
          } else {
            console.log(data);
            dispatch({ type: TASKS_MANAGMENT_REQUEST_CREATE_TASK_FAIL });
          }            
    }).catch((error) => {
        dispatch({ type: TASKS_MANAGMENT_REQUEST_CREATE_TASK_FAIL });
        console.log(error);
    })
  };

 export const resetTaskCreationFlag = () => dispatch=> (dispatch({
    type: TASKS_MANAGMENT_REQUEST_CREATE_TASK_RESET
 } ));


