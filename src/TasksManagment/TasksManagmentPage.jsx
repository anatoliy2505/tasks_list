import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Link, TasksCreationList} from '../components';
import FormSendTask from './modules/TaskCreationForm/FormComponent';

import {
  taskManagmentTaskCreationIsLoadingSelector,
  taskManagmentTaskCreationIsLoadedSelector
} from './services/tasksInterectionService/selectors';

import {
  loginManagmentIsAuthSelector
} from '../LoginManagment/service/loginService/selectors';

import {
  tasksRootManagmentTasksListIsLoadingSelector,
  tasksRootManagmentTasksListSelector,

  tasksRootManagmentTasksTotalPageSelector,  
  tasksRootManagmentTasksCurrentPageSelector,

  tasksRootManagmentTasksSortOfFieldSelector,
  tasksRootManagmentTasksSortOfDirectionSelector
} from '../redux/selectors';

import {
  createTask, 
  resetTaskCreationFlag
} from './services/tasksInterectionService/actions';

import { getTaskList } from '../redux/actions';

class TasksManagmentPage extends Component {

  constructor(props) {
    super(props);
    this.getTaskListChanged = this.getTaskListChanged.bind(this);
    this.getTaskListFiltred = this.getTaskListFiltred.bind(this);
  }

  getTaskListChanged(e) {
    const newDataObj = {
      page: this.props.tasksCurrentPage,
      sort_field: this.props.tasksSortOfField,
      sort_direction: this.props.tasksSortOfDirection
    }
    this.getTaskListFiltred(newDataObj);
  }

  getTaskListFiltred({page, sort_field, sort_direction}) {
    this.props.getTaskList(page, sort_field, sort_direction);
  }

  componentDidMount(){
    this.props.getTaskList();        
  }

  render() {
    const {
      tasksListIsLoading, 
      tasksList, 
      tasksTotalPages,
      tasksCurrentPage,

      tasksSortOfField,
      tasksSortOfDirection,

      taskCreationIsLoaded, 
      taskCreationIsLoading, 
      resetTaskCreationFlag,
      createTask,

      isAdmin
    } = this.props;
    return (
        <React.Fragment>
          {isAdmin ? <Link link="/admin">Перейти в личный кабинет</Link> : <Link link="/login">Войти в личный кабинет</Link>}

          <FormSendTask getTaskListChanged={this.getTaskListChanged}
            taskCreationIsLoaded={taskCreationIsLoaded}
            createTask={createTask}
            disabled={taskCreationIsLoading}
            resetTaskCreationFlag={resetTaskCreationFlag}/>

          <TasksCreationList
            title="Список задач" 
            tasksListIsLoading={tasksListIsLoading}
            tasksList={tasksList}
            tasksTotalPages={tasksTotalPages}
            tasksCurrentPage={tasksCurrentPage}

            tasksSortOfField={tasksSortOfField}
            tasksSortOfDirection={tasksSortOfDirection}
            
            getTaskListFiltred={this.getTaskListFiltred}
            />

        </React.Fragment>
      )
  }
    
}

export default connect((state) => {
  return {
    tasksListIsLoading:  tasksRootManagmentTasksListIsLoadingSelector(state),
    tasksList:  tasksRootManagmentTasksListSelector(state),

    tasksTotalPages: tasksRootManagmentTasksTotalPageSelector(state),
    tasksCurrentPage: tasksRootManagmentTasksCurrentPageSelector(state),

    taskCreationIsLoading:  taskManagmentTaskCreationIsLoadingSelector(state),
    taskCreationIsLoaded: taskManagmentTaskCreationIsLoadedSelector(state),

    tasksSortOfField:  tasksRootManagmentTasksSortOfFieldSelector(state),
    tasksSortOfDirection: tasksRootManagmentTasksSortOfDirectionSelector(state),

    isAdmin: loginManagmentIsAuthSelector(state),
  }; }, {
    getTaskList,

    createTask, 
    resetTaskCreationFlag
  } )(TasksManagmentPage)
  