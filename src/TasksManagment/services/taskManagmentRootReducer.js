import { combineReducers } from 'redux';

import {taskInterectionServiceReducer} from './tasksInterectionService/reducer'

export const TASK_MANAGMENT_REDUCER_NAMESPACES = {
    taskManagmentRoot: "taskManagment",
    tasksInteraction: "tasksInteraction",
    filters: "filters",
    sorts: "sorts"
}


export const taskManagmentRootReducer = combineReducers({
    [TASK_MANAGMENT_REDUCER_NAMESPACES.tasksInteraction]: taskInterectionServiceReducer,
});


