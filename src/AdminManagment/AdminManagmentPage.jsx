import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import {Link, TasksCreationList} from '../components';

import {
  tasksRootManagmentTasksListIsLoadingSelector,
  tasksRootManagmentTasksListSelector,

  tasksRootManagmentTasksTotalPageSelector,  
  tasksRootManagmentTasksCurrentPageSelector,

  tasksRootManagmentTasksSortOfFieldSelector,
  tasksRootManagmentTasksSortOfDirectionSelector
} from '../redux/selectors';

import {adminManagmentTaskChangeFailSelector} from './services/taskChangeService/selectors';

import {loginManagmentIsAuthSelector} from '../LoginManagment/service/loginService/selectors';

import { getTaskList } from '../redux/actions';

class AdminManagmentPage extends Component {

  constructor(props) {
    super(props);
    
    this.getTaskListChanged = this.getTaskListChanged.bind(this);
    this.getTaskListFiltred = this.getTaskListFiltred.bind(this);
  }

  getTaskListChanged(e) {
    const newDataObj = {
      page: this.props.tasksCurrentPage,
      sort_field: this.props.tasksSortOfField,
      sort_direction: this.props.tasksSortOfDirection
    }
    this.getTaskListFiltred(newDataObj);
  }

  getTaskListFiltred({page, sort_field, sort_direction}) {
    this.props.getTaskList(page, sort_field, sort_direction);
  }

  componentDidMount(){
    this.props.getTaskList();        
  }

  render() {

    return (
        <React.Fragment>
          
          {(!this.props.isAdmin || this.props.taskChangeFail) && <Redirect to='/logout' />}

          <Link link="/logout">Выйти</Link>

          <TasksCreationList
            {...this.props} 
            title="Список задач доступных для редактирования"

            getTaskListFiltred={this.getTaskListFiltred}
            getTaskListChanged={this.getTaskListChanged}
            />

        </React.Fragment>
      )
  }
    
}

export default connect((state) => {
  return {
    tasksListIsLoading:  tasksRootManagmentTasksListIsLoadingSelector(state),
    tasksList:  tasksRootManagmentTasksListSelector(state),

    tasksTotalPages: tasksRootManagmentTasksTotalPageSelector(state),
    tasksCurrentPage: tasksRootManagmentTasksCurrentPageSelector(state),

    tasksSortOfField:  tasksRootManagmentTasksSortOfFieldSelector(state),
    tasksSortOfDirection: tasksRootManagmentTasksSortOfDirectionSelector(state),

    isAdmin: loginManagmentIsAuthSelector(state),
    taskChangeFail: adminManagmentTaskChangeFailSelector(state)
  }; }, {
    getTaskList
  } )(AdminManagmentPage)
  