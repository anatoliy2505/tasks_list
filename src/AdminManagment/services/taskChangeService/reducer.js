import {
    ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_START,
    ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_SUCCESS,
    ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_FAIL,
    ADMIN_MANAGMENT_REQUEST_CHANGE_TASKK_RESET    
    } from './actions';

const initialState={
    taskChangeIsLoading: false,
    taskChangeIsLoaded: false,
    taskChangeFail: false
}

export function taskChangeServiceReducer(state=initialState, action) {
    switch (action.type) {
        case ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_START:
          return {
            ...state,
            taskChangeIsLoading: true,
            taskChangeIsLoaded: false
          };
        case ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_SUCCESS:
          return {
            ...state,
            taskChangeIsLoading: false,
            taskChangeIsLoaded: true
          };
        case ADMIN_MANAGMENT_REQUEST_CHANGE_TASK_FAIL:
          return {
            ...state,
            taskChangeIsLoading: false,
            taskChangeIsLoaded: false,
            taskChangeFail: true
          };
        case ADMIN_MANAGMENT_REQUEST_CHANGE_TASKK_RESET: 
          return {
            ...state,
            taskChangeIsLoading: false,
            taskChangeIsLoaded: false,
            taskChangeFail: false
        };
        default: 
            return state;
    }
}
