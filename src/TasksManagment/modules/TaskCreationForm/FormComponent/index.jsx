import React, {Component} from 'react';

import openNotification from '../../../../utils/helpers/openNotification';
import validateHelpers from '../../../../utils/validate';

export default class FormSendTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskName: '',
            taskEmail: '',
            taskText: '',
            errors: {}
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
    }
    
    componentDidUpdate(prevProps) {
        if(prevProps.taskCreationIsLoaded 
            !== this.props.taskCreationIsLoaded
            && this.props.taskCreationIsLoaded) {
            this.props.getTaskListChanged();
            this.resetForm();
            openNotification({
                title: 'Успех!',
                text: 'Ваше задание успешно добавлено.',
                type: 'success'
            });
        }
    }

    resetForm(e) {
        this.props.resetTaskCreationFlag();
        this.setState({
            taskName: '',
            taskEmail: '',
            taskText: '',
            errors: {}
        });
    }

    handleSubmitForm(e) {
        e.preventDefault();
        const {taskName, taskEmail, taskText} = this.state,
            values = {
                username: taskName,
                email: taskEmail,
                text: taskText
            },
            validatedInput = () => {
                let errors = {};
                validateHelpers({errors, values});  
                return errors;      
            },
            errorsObj = validatedInput();

        if( Object.keys(errorsObj).length === 0 ) {
            this.props.createTask(values);
        } else {
            this.setState({errors: errorsObj})
        }        
    }

    handleInputChange(e) {
        let name = e.target.name,
            value = e.target.value;

        if ( name === "username" ||  name === "email" )
            value = value.trim;    
        this.setState({
            [name]: value
        });
    }

    render() {
        let {taskName, taskEmail, taskText, errors} = this.state;
        return (
            <div>
                <h1 className="title">Создать задачу</h1>
                <form className="main-form">
                    <div className="wrapper">
                        <input type="text" name="taskName" 
                            className="username" placeholder="Имя" 
                            onChange={this.handleInputChange} value={taskName}/>
                        {!errors.username ?  null : <div className='error'>{errors.username}</div>}
                    </div>

                    <div className="wrapper">
                        <input type="email" name="taskEmail" 
                            className="email" placeholder="Ваш e-mail" 
                            onChange={this.handleInputChange} value={taskEmail}/>
                        {!errors.email ? null : <div className='error'>{errors.email}</div>}
                    </div>

                    <div className="wrapper">
                        <input type="text" name="taskText" 
                            className="text" placeholder="Текст задания" 
                            onChange={this.handleInputChange} value={taskText}/>
                        {!errors.text ? null : <div className='error'>{errors.text}</div>}
                    </div>

                    <button disabled={this.props.disabled} onClick={this.handleSubmitForm}>
                        {
                            (!taskName.length || !taskEmail.length || !taskText.length) 
                                && "нельзя "
                        }добавить задание
                    </button>
                </form>            
            </div>
        )
    }
}


