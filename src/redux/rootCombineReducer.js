import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';

import {TASK_MANAGMENT_REDUCER_NAMESPACES, taskManagmentRootReducer} from '../TasksManagment/services/taskManagmentRootReducer';
import {LOGIN_MANAGMENT_REDUCER_NAMESPACES, loginManagmentRootReducer} from '../LoginManagment/service/loginManagmentRootReducer';
import {ADMIN_MANAGMENT_REDUCER_NAMESPACES, adminManagmentRootReducer} from '../AdminManagment/services/adminManagmentRootReducer';
import {tasksRootReducer} from './reducer';

export const ROOT_TASKS_MANAGMENT_REDUCER_NAMESPACES = {
    rootTasksManagment: "rootTasksManagnet"
}

export const createRootReducers = history => combineReducers({
    router: connectRouter(history),
    [ROOT_TASKS_MANAGMENT_REDUCER_NAMESPACES.rootTasksManagment]: tasksRootReducer,
    [TASK_MANAGMENT_REDUCER_NAMESPACES.taskManagmentRoot]: taskManagmentRootReducer,
    [LOGIN_MANAGMENT_REDUCER_NAMESPACES.loginManagmentRoot]: loginManagmentRootReducer,
    [ADMIN_MANAGMENT_REDUCER_NAMESPACES.adminManagmentRoot]: adminManagmentRootReducer,
});
