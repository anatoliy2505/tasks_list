import React from 'react';
import ReactPaginate from 'react-paginate';

import './TasksCreationList.scss'; 
import loader from './img/loading.gif';

import TaskChengeFields from '../../AdminManagment/modules/TaskChengeFields'

const TasksCreationList = ({
    tasksCurrentPage,    
    tasksTotalPages,

    tasksSortOfDirection, 
    tasksSortOfField, 
    getTaskListFiltred,

    tasksListIsLoading,
    tasksList,
    
    title,

    isAdmin=false  
}) =>  {
    
    const onChangeSortFieldSelect = e => {
        const newDataObj = {
            page: tasksCurrentPage,
            sort_field: e.target.value,
            sort_direction: tasksSortOfDirection
        }
        getTaskListFiltred(newDataObj);        
        
    } 

    const onChangeSortDirectionSelect = e => {
        const newDataObj = {
            page: tasksCurrentPage,
            sort_field: tasksSortOfField,
            sort_direction: e.target.value
        }
        getTaskListFiltred(newDataObj);
    }
    
    const handlePageClick = ({selected}) => {
        const newDataObj = {
            page: (selected+1),
            sort_field: tasksSortOfField,
            sort_direction: tasksSortOfDirection
        }
        getTaskListFiltred(newDataObj);
    }
    
     return (
        <div className="todo-list">

            <h1 className="title">{title}</h1>

            <div className="sort">
                <h3>Выберите условия для сортировки заданий</h3>                    
            </div>
            <label htmlFor="sort_field">Сортировать по полю
                <select name='sort_field' value={tasksSortOfField} className='select' onChange={onChangeSortFieldSelect}>
                    <option value="id">По умолчанию</option>                        
                    <option value="username">Имя</option>                        
                    <option value="email">E-mail</option>                        
                    <option value="status">Статус</option>                        
                </select>
            </label>
            <label htmlFor="sort_direction">Сортировать по направлению
                <select name='sort_direction' value={tasksSortOfDirection} className='select' onChange={onChangeSortDirectionSelect}>
                    <option value="asc">Прямое</option>                        
                    <option value="desc">Обратное</option>                       
                </select>
            </label>
                
            {
                tasksListIsLoading ? <div className="info">
                                <img className="loader" src={loader} alt="loader"/>
                                <span>Загрузка задач...</span>
                            </div>: 
                    tasksList && tasksList.length > 0 ?  tasksList.map(item => 
                        (<div key={item.id} className="todo">
                            <span className="todo__item">{item.username}</span>
                            <span className="todo__item">{item.email}</span> 

                            {!isAdmin ? 
                                (
                                    <div>
                                        <p className="todo__task">{item.text}</p>
                                        <span className="todo__item">{item.status === 10 ? "Выполнено" : "Не выполнено"}</span>
                                        {item.status === 5 ? <span className="todo__item">Изменено администратором</span> : null}                                    
                                    </div>
                                ) : 
                                    
                                    <TaskChengeFields text={item.text} status={item.status} id={item.id}/>
                                }

                        </div>)
                        ) : <div className="info">
                                <span>Список задач пуст...</span>
                            </div>
                    
            }
            {
                (tasksTotalPages > 1) && (<ReactPaginate
                    previousLabel={'Назад'}
                    nextLabel={'Вперёд'}
                    breakLabel={'...'}
                    initialPage={(tasksCurrentPage-1)}
                    breakClassName={'break-me'}
                    pageCount={tasksTotalPages}
                    marginPagesDisplayed={5}
                    pageRangeDisplayed={2}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />)
            }
        </div>    
        )
    
}

export default TasksCreationList;