import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import validateHelpers from '../../../utils/validate';
import openNotification from '../../../utils/helpers/openNotification';

import {
    loginManagmentSignInStartedSelector, 
    loginManagmentSignInFinishedSelector,
    loginManagmentIsAuthSelector
} from '../../service/loginService/selectors';

import {signIn, resetLoginFinishedFlag} from '../../service/loginService/actions'

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            errors: {}
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
    }

    componentDidUpdate(prevProps) {
        if(
            (prevProps.singInIsFinished 
            !== this.props.singInIsFinished
            && this.props.singInIsFinished) &&
            (prevProps.isAdmin 
            !== this.props.isAdmin
            && this.props.isAdmin)
        )  {                
            this.props.resetLoginFinishedFlag();
            this.resetForm();       
            openNotification({
                title: 'Отлично!',
                text: 'Вы успешно авторизовались.',
                type: 'success'
            })     
        }
    }

    resetForm(e) {
        this.setState({
            login: '',
            password: '',
            errors: {}
        });
    }

    handleSubmitForm(e) {
        e.preventDefault();
        const {login, password} = this.state,
        values = {
            password: password,
            login: login
        },
        validatedInput = () => {
            let errors = {};
            validateHelpers({errors, values});  
            return errors;      
        },
        errorsObj = validatedInput();

        if( Object.keys(errorsObj).length === 0 ) {
            this.props.signIn(values);
        } else {
            this.setState({errors: errorsObj})
        }        
    }

    handleInputChange(e) {  
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        let {login, password, errors} = this.state;

        return (
            <div>                
                {this.props.isAdmin ? <Redirect to='/admin' /> : null}
                <h1 className="title">Вход в личный кабинет</h1>
                <form className="main-form">
                    <div className="wrapper">
                        <input type="text" name="login" 
                            className="login" placeholder="Введите login" 
                            onChange={this.handleInputChange} value={login}/>
                        {!errors.login ? null : <div className='error'>{errors.login}</div>}
                    </div>

                    <div className="wrapper">
                        <input type="password" name="password" 
                            className="password" placeholder="Введите пароль" 
                            onChange={this.handleInputChange} value={password}/>
                        {!errors.password ? null : <div className='error'>{errors.password}</div>}
                    </div>

                    <button disabled={this.props.loginManagmentSignInStartedSelector} onClick={this.handleSubmitForm}>
                        {
                            (!login.length || !password.length) 
                               ? "Заполните поля" : 'Войти'
                        }
                    </button>
                </form>            
            </div>
        )
    }
}

export default connect((state)=>{
    return {
        singInIsSrarted: loginManagmentSignInStartedSelector(state),
        singInIsFinished: loginManagmentSignInFinishedSelector(state),
        isAdmin: loginManagmentIsAuthSelector(state)
    }
}, {signIn, resetLoginFinishedFlag})(LoginForm);

