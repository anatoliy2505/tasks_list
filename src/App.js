import React from 'react';
import {Route} from 'react-router-dom';

import {Page} from './components';
import TasksManagmentPage from './TasksManagment/TasksManagmentPage';
import AdminManagmentPage from './AdminManagment/AdminManagmentPage';
import LoginPage from './LoginManagment/LoginPage';
import LogoutPage from './LoginManagment/LogoutPage';

function App() {
  return (
    <Page>
      <Route exact path={['/','/tasks-managment']} component={TasksManagmentPage} />
      <Route path='/admin' component={AdminManagmentPage} />
      <Route path='/login' component={LoginPage} />
      <Route path='/logout' component={LogoutPage} />
    </Page>
  );
}

export default App;
