import {START, SUCCESS, FAIL} from '../common/constans';
import axios from 'axios';

import openNotification from '../utils/helpers/openNotification';

export const ROOT_TASKS_MANAGMENT ="ROOT_TASKS_MANAGMENT";
export const REQUEST_GET_TASK_LIST = "REQUEST_GET_TASK_LIST";
export const ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST = ROOT_TASKS_MANAGMENT+REQUEST_GET_TASK_LIST;
export const ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_START = ROOT_TASKS_MANAGMENT+REQUEST_GET_TASK_LIST+START;
export const ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_SUCCESS = ROOT_TASKS_MANAGMENT+REQUEST_GET_TASK_LIST+SUCCESS;
export const ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_FAIL = ROOT_TASKS_MANAGMENT+REQUEST_GET_TASK_LIST+FAIL;

 export const getTaskList = (page=1, sort_field='id', sort_direction='asc') => dispatch => {
    dispatch({ type: ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_START });
    return axios({
        method: 'get',
        baseURL: 'https://uxcandy.com/~shapoval/test-task-backend/v2',
        url: '/?developer=Anatoliy&page='+page+'&sort_direction='+sort_direction+'&sort_field='+sort_field,
        proxy: {
            host: 'https://uxcandy.com'
          }
      }).then(({data}) => {      
          if(data.status === 'ok') {
            dispatch({ 
                type: ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST, 
                tasks: data.message.tasks, 
                totalTask: data.message.total_task_count,
                currentPage: page,
                sortField: sort_field,
                sortDirection: sort_direction   
            });
            dispatch({ type: ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_SUCCESS });            
            
          } else {
            dispatch({ type: ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_FAIL });
            openNotification({
              title: 'Ошибка!',
              text: 'Неудалось получить список заданий.',
              type: 'error'
            });
          }       
       
    }).catch((error) => {
        dispatch({ type: ROOT_TASKS_MANAGMENT_REQUEST_GET_TASK_LIST_FAIL });
        openNotification({
          title: 'Ошибка!',
          text: 'Неудалось получить список заданий.',
          type: 'error'
        });
    })
};
