import {createSelector} from 'reselect';

export const adminManagmentRootSelector = state => state.adminManagment.taskChange;

export const adminManagmentTaskChangeIsLoadingSelector = createSelector(
    adminManagmentRootSelector,
    state => Boolean(state && state.taskChangeIsLoading),
  );  
  
export const adminManagmentTaskChangeIsLoadedSelector = createSelector(
  adminManagmentRootSelector,
  state => Boolean(state && state.taskChangeIsLoaded),
);

export const adminManagmentTaskChangeFailSelector = createSelector(
  adminManagmentRootSelector,
  state => Boolean(state && state.taskChangeFail),
);